<!-- Body text: Size 4 -->

# 🍪 About me

<font size="4">

Hey hey, welcome! My name is Cookie, I'm a doggo on the Internet who loves computers and coding (hence, the existence of this account, hehe). I also love drawing, streaming, and playing games with my friends.

</font>



# 🔗 Social links

<font size="4">

You can find my socials here:

+ [My Twitter](https://twitter.com/CookieCollie)
+ [My Twitch](https://www.twitch.tv/cookiecollie)
+ [My Discord Server](https://discord.gg/YrhgDeSZDX)

</font>

# 💸 Tips

<font size="4">

If you love what I do and want to support, you can tip me through my Ko-fi!

[My Ko-fi](https://ko-fi.com/cookiecollie)

</font>

# Credits

<font size="4">

+ Profile picture by [@MegasArts](https://twitter.com/megasarts)

</font>